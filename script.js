/* 
Создайте приложение, в котором пользователь будет отправлять запросы по адресу: ‘https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json’ и получать текущий курс гривны по отношению к иностранным валютам. Отфильтруйте полученный список по уровню курса - только те элементы, у которых курс больше 25грн. В случае ошибки – отобразите ее пользователю в теле документа. В случае успеха – отобразите вывод данных в виде таблицы. 
*/

const btn = document.querySelector('button');
const container = document.querySelector('.container');


btn.addEventListener('click', getRate);

function getRate() {
    let requestRate = new XMLHttpRequest();
    requestRate.open("GET", "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json");
    requestRate.addEventListener('load', () => {
        let responce = JSON.parse(requestRate.responseText);
        showRate(responce);
    })
    requestRate.send();
}

function showRate(responce) {
    responce.sort((a,b) => a.rate > b.rate ? -1 : 1);
    responce.forEach(el => {
        if(el.rate > 25){
            const currencyName = document.createElement('div');
            currencyName.classList.add('currency-name');
            currencyName.textContent = el.txt;
            const currencyRate = document.createElement('div');
            currencyRate.classList.add('currency-rate');
            currencyRate.textContent = `${el.rate}UAH`;
            const hr = document.createElement('hr');
            container.appendChild(currencyName);
            container.appendChild(currencyRate);
            container.appendChild(hr);
        }
    });
}
